#!/bin/bash
#This is a script to setup the Lufticus1000 software on Raspberry pi. Required Hardware:
#Original 7" touch Display
#DHT22 Sensor
#MH-Z19 NDIR infrared carbon dioxide co2 gas sensor

apt update
apt dist-upgrade
apt update
apt dist-upgrade
pip3 install Adafruit_DHT
mkdir .Lufticus
cd .Lufticus
git init
git clone https://gitlab.com/moritzschachner/lufticus1000.git
